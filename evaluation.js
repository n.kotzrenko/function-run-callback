/**
 * Return only the successful callbacks - those that didn't crash for some reason
 * @param {Callback[]} callbacks Callbacks
 * @returns {CallbackResult[]}
 */
exports.runCallbacks = async callbacks => {
    const result = []
    for (callback of callbacks) {
        try {
            const runResult = await run(callback)
            result.push(runResult)
        } catch(err) {
            console.error(err)
        }
    }
    return result
}

/**
 * Run the given callback
 * @param {Callback} callback The callback to run
 * @returns {Promise<ExecutionResult>}
 */
const run = async callback => {
    // Declare an exports object to simulate requireJS type modules
    const exports = {}

    // Run the module that contains the callback function.
    // It is ran the same way as a 'standard' pre ES6 module for
    // simplicities sake.
    console.log(`Evaluating callback id ${callback.id}`)
    evaluate(exports, callback.callback)

    // Run the function code and keep it's result - defaults to undefined
    // in case the fn property is unset for any reason - interface for callbacks 
    console.log('Executing evaluated module')
    const result = await (async () => {
        if (exports.fn) return await exports.fn(JSON.parse(callback.params))
    })()

    console.log('Execution result is')
    console.log(result)
    
    const result = {
        id: callback.id,
        callback: callback.callback,
        lastRun: new Date(),
        result
    }
    console.log('Callback execution result is')
    console.log(result)
    
    return result
}

/**
 * Evaluate the callback module and bind the results onto the exports object
 * @param {object} exports Exports object for the callback module
 * @param {string} callback The module to evaluate
 */
const evaluate = (exports, callback) => {
    eval(callback)
}