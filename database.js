require('./types')
const mySql = require('mysql')
const allSettled = require('promise.allsettled')
const {toSqlDateTime} = require('./date')

/**
 * Get all callbacks at the given cron time
 * @param {Environment} env Lambda environment
 * @param {string} jobname The jobname of the scheduled run
 * @returns {Promise<Callback[]>}
 */
exports.callbacksAt = async (env, jobname) => {
    const cronTime = jobnameToCronTime(jobname)
    const conn = await connection(env)
    return await selectCallbacks(conn, cronTime)
}

/**
 * Select callbacks from datatabse
 * @param {import('mysql').Connection} conn Database connection 
 * @param {string} cronTime String representing cron expression
 */
const selectCallbacks = (conn, cronTime) => new Promise((resolve, reject) => {
    conn.query(
        'SELECT id, callback, params FROM funcs WHERE cron_time = ?;',
        [cronTime],
        (err, results) => {
            conn.end()
            if (err) return reject(err)
            return resolve(results)
        } 
    )
})

/**
 * Convert jobname into cron time
 * @param {string} jobname Jobname
 */
const jobnameToCronTime = jobname => jobname.replace(/_/g, ' ').replace(/-/g, '\*')

/**
 * Save the run results in the database
 * @param {Environment} env
 * @param {ExecutionResult[]} results 
 */
exports.saveResults = async (env, results) => {
    const conn = await connection(env)
    const saveResults = await allSettled(results.map(result => updateResult(conn, result)))
    saveResults.filter(
            result => result.status === 'rejected'
        ).map(
            rejected => rejected.reason
        ).forEach(
            reason => {
                console.error(`failed to update callback id ${reason.id}`)
                console.error(reason)
            }
        )
    conn.end()
}

/**
 * 
 * @param {import('mysql').Connection} conn Database connection
 * @param {ExecutionResult} result Execution result of a callback
 * @returns {Promise<void>}
 */
const updateResult = (conn, result) => new Promise((resolve, reject) => {
    console.log('Updating callback result')
    console.log(result)
    conn.query(
        'UPDATE funcs SET last_run = ?, result = ? WHERE id = ?;',
        [toSqlDateTime(result.lastRun), JSON.stringify(result.result), result.id],
        err => {
            if (err) return reject(err)
            return resolve()
        }
    )
})

/**
 * Open a connection to the database
 * @param {Environment} env
 * @returns {Promise<import('mysql').Connection>}
 */
const connection = env => new Promise((resolve, reject) => {
    const conn = mySql.createConnection({
        user: env.dbUser,
        password: env.dbPassword,
        database: env.dbName,
        socketPath: env.dbSocketPath
    })
    conn.connect(err => {
        if (err) {
            conn.end()
            return reject(err)
        }
        return resolve(conn)
    })
})