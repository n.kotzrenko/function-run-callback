require('./types')
const {translateEnvironment} = require('./env')
const {callbacksAt, saveResults} = require('./database')
const {runCallbacks} = require('./evaluation')

exports.runCallback = async (req, res) => {
    try {
        const env = translateEnvironment()
        const params = extractRequestParams(req)

        console.log('Seeking callbacks to run')
        const callbacks = await callbacksAt(env, params.jobname)
        
        console.log('Start running found callbacks')
        const results = await runCallbacks(callbacks)

        console.log('Start saving results into database')
        await saveResults(env, results)
        
        res.status(200).end()
    } catch (err) {
        console.error(err)
        res.sendStatus(500).end()
    }
}

/**
 * Extract request parameters
 * @param {Express.Request} req Request object from the Express framework
 * @returns {RequestParameters}
 */
const extractRequestParams = req => {
    console.log('Request body is')
    const body = req.body.toString() 
    console.log(body)
    return JSON.parse(body)
}
