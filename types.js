/**
 * @typedef {{
 *  jobname: string
 * }} RequestParameters
 */

 /**
  * @typedef {{
  *  dbHost: string,
  *  dbUser: string,
  *  dbPassword: string,
  *  dbName: string,
  *  dbSocketPath: string
  * }} Environment
  */

/**
 * @typedef {{
 *  id: number,
 *  callback: string,
 *  params: string
 * }} Callback
 */

/**
 * @typedef {{
 *  id: number,
 *  callback: string,
 *  lastRun: Date,
 *  result: any
 * }} ExecutionResult
 */